import os

exts_list = {
    "CSharp": ["cs"],
    "Java": ["java"],
    "JavaScript": ["mjs", "js", "ts"],
    "Json": ["json"],
    "Objective-C": ["m", "mm"],
    "Proto": ["proto", "protodevel"],
    "TableGen": ["td"],
    "TextProto": ["textpb", "pb.txt", "textproto", "asciipb"],
    "Verilog": ["sv", "svh", "v", "vh"],
    "C": ["c"],
    "C++": ["cpp"],
}


def analyze_args(args: list) -> list[str]:
    """Renvoie la liste des fichiers"""
    if len(args) > 1:
        raise Exception("Trop d'arguments renseignés.")

    if not len(args):
        raise Exception("Aucun argument renseigné.")

    if os.path.isdir(args[0]):
        args = iterate_over_directory(args[0])
    else:
        if not list(filter(lambda file: os.path.exists(file), args)):
            raise Exception("Le fichier n'existe pas.")

    files = list(filter(check_ext, args))

    if not files:
        raise Exception(f"Aucun fichiers supporté ({', '.join(args)}).")

    return files


def check_ext(filename: str) -> bool:
    """Vérifie qu'un fichier est supporté en se basant sur son extension"""
    supported_exts = tuple(
        map(
            lambda ext: "." + ext,
            [ext for exts in exts_list.values() for ext in exts],
        )
    )

    return filename.endswith(supported_exts)


def iterate_over_directory(dirname: str) -> list[str]:
    """Trouve tout les fichiers d'un dossier"""
    files = next(os.walk(dirname))[2]

    return list(map(lambda file: os.path.join(dirname, file), files))
