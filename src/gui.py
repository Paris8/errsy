from tkinter import Button, Frame, Label, Menu, Tk, Toplevel
from tkinter.filedialog import askdirectory, askopenfile
from tkinter.messagebox import showerror

from src.clangformat import Style, clang_format, parse_clang_format_output
from src.errors import ClangError
from src.utils import analyze_args, exts_list


class GUI:
    def __init__(self) -> None:
        self.name = "Errsy"
        self.parent = Tk()
        self.parent.resizable(False, False)
        self.f = Frame(self.parent)

        self.columnspan = 3  # taille utilisé en largeur pour les éléments
        self.style = Style.FILE  # style utilisé
        self.current_location = ""  # ce qu'on regarde

    def start(self) -> None:
        """Affiche la fenêtre"""
        self._main_screen()
        self.parent.title(self.name)
        self.parent.mainloop()

    def _open_file(self, start: int) -> None:
        try:
            chemin = askopenfile(
                title=f"Choisir un fichier - {self.name}",
                filetypes=[("Tous les fichiers", "*")]
                + [
                    (langage, f"*.{ext}")
                    for langage, exts in exts_list.items()
                    for ext in exts
                ],
            )
        except AttributeError:
            showerror(f"Erreur - {self.name}", "Impossible de trouver le fichier")
        else:
            if chemin:
                self.current_location = chemin.name
                return self._analyse(start, chemin.name)

    def _open_directory(self, start: int) -> None:
        try:
            chemin = askdirectory()
        except AttributeError:
            showerror(f"Erreur - {self.name}", "Impossible de trouver le dossier")
        else:
            self.current_location = chemin
            return self._analyse(start, chemin)

    def _add_styles_menu_bar(self, start: int) -> None:
        def ev(s: str) -> str:
            """Récupère la valeur du style"""
            return eval(compile(f"Style.{s}", "<string>", "eval"))

        for style in sorted(Style.PRESETS_LIST):
            self.style_menu.add_command(
                label=f"{style} (en cours)" if ev(style) == self.style else style,
                command=lambda s=ev(style): self._update_style(s, start),
            )

    def _update_style(self, new_style: str, start: int) -> None:
        """Met à jour la barre de menu de selection des styles + la fenêtre au besoin"""
        if new_style != self.style:
            self.style_menu.delete(0, len(Style.PRESETS_LIST))

            self.style = new_style

            self._add_styles_menu_bar(start)

            if len(self.current_location):
                self._analyse(start, self.current_location)

    def _main_screen(self) -> None:
        """Écran principal"""
        Label(
            self.parent,
            text=f"{self.name} est une application qui permet de lire "
            "plus facilement clang-format",
        ).grid(column=0, row=0, columnspan=self.columnspan)

        start = 2
        Button(
            self.parent,
            text="Ouvrir un fichier",
            command=lambda: self._open_file(start),
        ).grid(column=0, row=1)
        Button(
            self.parent,
            text="Ouvrir un dossier",
            command=lambda: self._open_directory(start),
        ).grid(column=self.columnspan - 1, row=1)

        # Barre de menu
        menu_bar = Menu(self.parent)
        self.parent.config(menu=menu_bar)

        # Menu "Style"
        file_menu = Menu(menu_bar, tearoff=0)
        file_menu.add_separator()
        self.style_menu = Menu(file_menu, tearoff=0)
        self._add_styles_menu_bar(start)
        file_menu.add_cascade(label="Sélection style", menu=self.style_menu)

        # Menu "Fermer"
        file_menu.add_separator()
        file_menu.add_command(
            label="Fermer",
            command=self.parent.destroy,
        )

        # Tout ça dans l'onglet "Fichier"
        menu_bar.add_cascade(label="Fichier", menu=file_menu)

    def _show_info(self, name_err: str, msg: str) -> None:
        """Fenêtre info qui affiche le message d'avertissement"""
        title = f"{name_err} - {self.name}"
        info = Toplevel()
        info.resizable(False, False)
        info.title(title)
        Label(info, text=msg).pack()

        width = max([len(line) for line in [title] + msg.splitlines()]) * 12
        height = (3 + msg.count("\n")) * 10
        info.geometry(f"{width}x{height}")

    def _analyse(self, start_row: int, path: str) -> None:
        """Analyse les données"""
        try:
            files = analyze_args([path])
        except Exception as e:
            showerror(f"Erreur - {self.name}", str(e))
        else:
            data = []
            for file in files:
                try:
                    clang_format_output = clang_format(file, self.style)
                except Exception as e:
                    showerror(f"Erreur - {self.name}", str(e))
                    return
                else:
                    data += parse_clang_format_output(clang_format_output)
            self._pager(1, data, start_row)

    def _reset_frame(self) -> None:
        """Reset frame"""
        self.f.destroy()
        self.f = Frame(self.parent)
        self.f.grid(columnspan=self.columnspan)

    def _pager(self, num_page: int, errors: list[ClangError], start_row: int) -> None:
        """Affiche les avertissement sous forme de pages"""
        self._reset_frame()

        idx = start_row
        elem_per_page = 10
        for error in errors[elem_per_page * (num_page - 1) : elem_per_page * num_page]:
            if not error.clean:
                Button(
                    self.f,
                    text=f"Avertissement dans {error.filename} "
                    f"à la ligne {error.line_number}, "
                    f"caractère {error.column_number}.",
                    borderwidth=0,
                    bg="SandyBrown",
                    activebackground="Goldenrod",
                    command=lambda e=error: self._show_info(
                        str(e.warning_message), str(e.warning_content)
                    ),
                ).grid(column=0, row=idx, columnspan=self.columnspan)
                idx += 1
            else:
                Label(
                    self.f,
                    bg="MediumSpringGreen",
                    text=f"Aucun avertissement trouvé dans {error.filename}.",
                ).grid(column=0, row=idx, columnspan=self.columnspan)
                idx += 1

        page_max = 1 + len(errors) // elem_per_page
        if page_max > 1:
            Button(
                self.f,
                text="< Page précédente",
                state="disabled" if num_page <= 1 else "normal",
                command=lambda: self._pager(num_page - 1, errors, start_row),
            ).grid(column=0, row=idx)
            Label(self.f, text=f"Page {num_page}/{page_max}").grid(column=1, row=idx)
            Button(
                self.f,
                text="Page suivante >",
                state="disabled" if num_page >= page_max else "normal",
                command=lambda: self._pager(num_page + 1, errors, start_row),
            ).grid(column=2, row=idx)
