from typing import Union


class ClangError:
    def __init__(
        self,
        filename: str,
        line_number: Union[int, None] = None,
        column_number: Union[int, None] = None,
        warning_message: Union[str, None] = None,
        warning_content: Union[str, None] = None,
    ) -> None:
        self._filename = filename
        self._line_number = line_number
        self._column_number = column_number
        self._warning_message = warning_message
        self._warning_content = warning_content

    @property
    def filename(self) -> str:
        """Dans quel fichier se situe l'erreur"""
        return self._filename

    @property
    def line_number(self) -> Union[int, None]:
        """Sur quelle ligne se situe l'erreur"""
        return self._line_number

    @property
    def column_number(self) -> Union[int, None]:
        """Où se situe l'erreur sur la ligne"""
        return self._column_number

    @property
    def warning_message(self) -> Union[str, None]:
        """Quelle est le nom de l'erreur"""
        return self._warning_message

    @property
    def warning_content(self) -> Union[str, None]:
        """Quelle est le contenu de l'erreur"""
        return self._warning_content

    @property
    def clean(self) -> bool:
        """Vérifie s'il y a des erreurs"""
        return self._warning_message is None
