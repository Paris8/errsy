import re
import subprocess
import sys

from src.errors import ClangError


class Style:
    """Preset of coding style"""

    FILE = "file"  # .clang-format
    LLVM = "LLVM"
    GNU = "GNU"
    Google = "Google"
    Chromium = "Chromium"
    Microsoft = "Microsoft"
    Mozilla = "Mozilla"
    WebKit = "WebKit"

    PRESETS_LIST = [k for k in locals().keys() if not k.startswith("_")]


def clang_format(file_path: str, style: str) -> tuple[str, str]:
    command = ["clang-format", "--dry-run", file_path, f"--style={style}"]
    try:
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, text=True)
    except subprocess.CalledProcessError as e:
        output = e.output
    except FileNotFoundError as e:
        print(f"Commande non trouvé : {e.filename}", file=sys.stderr)
        raise Exception(
            "clang est requis pour utilisé ce programme, installé-le via "
            f"votre gestionnaire de paquet : {e.filename}"
        )

    return file_path, output


def parse_clang_format_output(data) -> list[ClangError]:
    filename, output = data
    error_pattern = (
        r"(?P<filename>.+):(?P<line_number>\d+):(?P<column_number>\d+)"
        r": warning: code should be clang-formatted \[(?P<warning_message>.+)\]"
        r"\n(?P<warning_content>(.)*\n?(.)*)"
    )
    error_matches = re.finditer(error_pattern, output)

    errors = []
    for error_match in error_matches:
        filename = str(error_match.group("filename"))
        line_number = int(error_match.group("line_number"))
        column_number = int(error_match.group("column_number"))
        warning_message = str(error_match.group("warning_message"))
        warning_content = str(error_match.group("warning_content"))

        errors.append(
            ClangError(
                filename,
                line_number,
                column_number,
                warning_message[1:],
                warning_content,
            )
        )

    if not errors:
        return [ClangError(filename)]

    return errors
