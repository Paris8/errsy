# Errsy

GUI pour `clang-format`

## Pré-requis

- clang-format
  > Arch : `sudo pacman -S clang`
- python 3.9 minimum

## Usage

```shell
$ python3 main.py
```

---

Pour le développement :

```shell
$ pip install -r requirements.txt
$ cp githooks/* .git/hooks/
```
